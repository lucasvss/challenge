import React from "react";

import api from "../../services/api";

import {Table, Container, Row, Col, Button, Modal, Form} from "react-bootstrap";
import { BsFillTrash2Fill, BsPencilSquare } from "react-icons/bs";

import { getUser } from "../../services/auth";
import MaskedFormControl from 'react-bootstrap-maskedinput';

const { cpf } = require('cpf-cnpj-validator');

class UserTable extends React.Component {

  constructor(props) {
    super(props);
    this.state = { users: [], modal: false, user: {}};
  }
  
  componentDidMount() {
    const user = JSON.parse(getUser());
    const encodedAuth = btoa(user.credentials);
    if (encodedAuth) {
      const config = {
        headers: {
          'Authorization': `Basic ${encodedAuth}`
        }
      };

      api.get("/api/users", config).then(response => {
        this.setState({users: response.data});
        console.log(this.state.users); 
      }).catch(error => {
        console.log(error);
      });
    }
  }

  toggle = () => {
    this.setState({ modal: !this.state.modal });
  }

  updateUser = (user, modal) => {
    const dateOfBirth = user.dateOfBirth ? this.formatDate(user.dateOfBirth) : "";
    this.setState({user, modal});
    this.setState(prevState => ({user: {...prevState.user, dateOfBirth: dateOfBirth}}));
  }

  formatDate = (date) => {
    return new Intl.DateTimeFormat('pt-br', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(new Date(date));
  }

  update = () => {
    const user = this.state.user;
    if(this.validateName(user.name) && 
        this.validateCPF(user.cpf) && 
        this.validateDateOfBirth(user.dateOfBirth) &&
        this.validateEmail(user.email)) {
          const day = user.dateOfBirth.substring(0, 2);
          const month = user.dateOfBirth.substring(3, 5);
          const year = user.dateOfBirth.substring(6, 10);
          user.dateOfBirth = new Date(month + '/' + day + '/' + year);  
          user.cpf = user.cpf.replace(/[\\.\-*]/g, '');
          const basicAuth = JSON.parse(getUser());
          const encodedAuth = btoa(basicAuth.credentials);
          if (encodedAuth) {
            const config = {
              headers: {
                'Authorization': `Basic ${encodedAuth}`
              }
            };
    
            api.put("/api/users", user, config)
            .then(response => {
              if(response) {
                window.location.reload(false);
              }
            })
            .catch(error => {
              console.log(error);
            });
          }
    }
  }

  validateName(value) {
    if(value === undefined || value === '') {
        this.setState({errors: {name: {isValid: false, message: 'Nome é de preenchimento obrigatório'}}});
        return false;
    }
    return true;
  }

  validateCPF(value) {
      if(value === undefined || value === '') {
        this.setState({errors: {cpf: {isValid: false, message: 'CPF é de preenchimento obrigatório'}}});
        return false;
      }

      if(!cpf.isValid(value)) {
        this.setState({errors: {cpf: {isValid: false, message: 'CPF inválido'}}});
        return false;
      }

      return true;
  }

  validateDateOfBirth(value) {
    if(value === undefined || value === '') {
        this.setState({errors: {dateOfBirth: {isValid: false, message: 'Data de nascimento é de preenchimento obrigatório'}}});
        return false;
    }
    return true;
  }

  validateEmail(value) {
    const regex = /\S+@\S+\.\S+/;

    if(value !== undefined && value !== "" && !regex.test(value)) {
      this.setState({errors: {email: {isValid: false, message: 'Email inválido'}}});
      return false;
    }
    return true;
  }

  remove = (user) => {
    const basicAuth = JSON.parse(getUser());
    const encodedAuth = btoa(basicAuth.credentials);
    if (encodedAuth) {
      const config = {
        headers: {
          'Authorization': `Basic ${encodedAuth}`
        }
      };

      api.delete("/api/users/"+user.id, config)
      .then(response => {
        if(response) {
          window.location.reload(false);
        }
      })
      .catch(error => {
        console.log(error);
      });
    }
  }

  renderRow = (row) => {
    console.log(row);
    return (
      <tr key={row.id}>
        <td>{row.id}</td>
        <td>{row.name}</td>
        <td>{row.cpf}</td>
        <td>{row.sex}</td>
        <td>{row.email}</td>
        <td>{row.dateOfBirth ? new Intl.DateTimeFormat('pt-br', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(new Date(row.dateOfBirth)) : ""}</td>
        <td>{row.naturalness}</td>
        <td>{row.nationality}</td>
        <td>{row.creationDate ? new Intl.DateTimeFormat('pt-br', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(new Date(row.creationDate)) : ""}</td>
        <td>{row.lastModification ? new Intl.DateTimeFormat('pt-br', { day: 'numeric', month: 'numeric', year: 'numeric' }).format(new Date(row.lastModification)) : ""}</td>
        <td><Button onClick={() => this.remove(row)}><BsFillTrash2Fill></BsFillTrash2Fill></Button></td>
        <td><Button onClick={() => this.updateUser(row, true)}><BsPencilSquare></BsPencilSquare></Button></td>
      </tr>
    )
  }

  render() {
    return (
      <Container >
        <Row>
          <Col>
          <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>CPF</th>
              <th>Sexo</th>
              <th>Email</th>
              <th>Data de nascimento</th>
              <th>Naturalidade</th>
              <th>Nacionalidade</th>
              <th>Data do registro</th>
              <th>Data da modificação</th>
              <th>Remover</th>
              <th>Atualizar</th>
            </tr>
          </thead>
          <tbody>
          {
              this.state.users.map(this.renderRow)
          }    
          </tbody>
        </Table>
          </Col>
        </Row>  
        <Modal show={this.state.modal} onHide={this.toggle} animation={true}>
          <Modal.Header closeButton>
            <Modal.Title>Atualizar usuário</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Nome</Form.Label>
              <Form.Control type="text" value={this.state.user.name} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, name: value}}))}}/>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>CPF</Form.Label>
              <MaskedFormControl type='text' mask='111.111.111-11' value={this.state.user.cpf} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, cpf: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Data de Nascimento</Form.Label>
              <MaskedFormControl type='text' mask='11/11/1111' value={this.state.user.dateOfBirth} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, dateOfBirth: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" value={this.state.user.email} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, email: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Naturalidade</Form.Label>
              <Form.Control type="text" value={this.state.user.naturalness} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, naturalness: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Nacionalidade</Form.Label>
              <Form.Control type="text" value={this.state.user.nationality} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, nationality: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formGridState">
              <Form.Label>Sexo</Form.Label>
              <Form.Control as="select" defaultValue={this.state.user.sex} onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, sex: value}}))}}>
                <option>F</option>
                <option>M</option>
              </Form.Control>
            </Form.Group>
          </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => this.toggle()}>Fechar</Button>
            <Button variant="primary" onClick={() => this.update()}>Atualizar</Button>
          </Modal.Footer>
        </Modal>
      </Container>
    );
  }
}

export default UserTable;