import React, { Component } from "react";

import api from "../../services/api";

import {Container, Button, Row, Col, Modal, Form} from "react-bootstrap";
import MaskedFormControl from 'react-bootstrap-maskedinput';
import { getUser, logout } from "../../services/auth";

const { cpf } = require('cpf-cnpj-validator');

class AddUser extends Component {
    
  constructor(props) {
    super(props);
    this.state = {
        modal: false,
        user: {sex: 'F'},
        errors: {
          name: {isValid: true, message: ""},
          email: {isValid: true, message: ""},
          dateOfBirth: {isValid: true, message: ""},
          cpf: {isValid: true, message: ""}
        }
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ modal: !this.state.modal });
  }

  save() {
    const user = this.state.user;
    if(this.validateName(user.name) && 
        this.validateCPF(user.cpf) && 
        this.validateDateOfBirth(user.dateOfBirth) &&
        this.validateEmail(user.email)) {
          const day = user.dateOfBirth.substring(0, 2);
          const month = user.dateOfBirth.substring(3, 5);
          const year = user.dateOfBirth.substring(6, 10);
          user.dateOfBirth = new Date(month + '/' + day + '/' + year);  
          user.cpf = user.cpf.replace(/[\\.\-*]/g, '');
          
          const basicAuth = JSON.parse(getUser());
          const encodedAuth = btoa(basicAuth.credentials);
          if (encodedAuth) {
            const config = {
              headers: {
                'Authorization': `Basic ${encodedAuth}`
              }
            };
    
            api.post("/api/users", user, config)
            .then(response => {
              if(response) {
                window.location.reload(false);
              }
            })
            .catch(error => {
              console.log(error);
            });
          }
    }
  }

  validateName(value) {
    if(value === undefined || value === '') {
        this.setState({errors: {name: {isValid: false, message: 'Nome é de preenchimento obrigatório'}}});
        return false;
    }
    return true;
  }

  validateCPF(value) {
      if(value === undefined || value === '') {
        this.setState({errors: {cpf: {isValid: false, message: 'CPF é de preenchimento obrigatório'}}});
        return false;
      }

      if(!cpf.isValid(value)) {
        this.setState({errors: {cpf: {isValid: false, message: 'CPF inválido'}}});
        return false;
      }

      return true;
  }

  validateDateOfBirth(value) {
    if(value === undefined || value === '') {
        this.setState({errors: {dateOfBirth: {isValid: false, message: 'Data de nascimento é de preenchimento obrigatório'}}});
        return false;
    }
    return true;
  }

  validateEmail(value) {
    const regex = /\S+@\S+\.\S+/;

    if(value !== undefined && value !== "" && !regex.test(value)) {
      this.setState({errors: {email: {isValid: false, message: 'Email inválido'}}});
      return false;
    }
    return true;
  }

  logout = () => {
    logout();
    window.location.reload(false);
  }

  render() {
    return (
      <Container>
          <Row>
            <Col sm="2">
            <Button variant="success" onClick={this.toggle}>
         Adicionar Usuário
        </Button>
        </Col>
        <Col sm="1">
            <Button onClick={() => this.logout()}variant="danger">
         Sair
        </Button>
        </Col>
        
  
        <Modal show={this.state.modal} onHide={this.toggle} animation={true}>
          <Modal.Header closeButton>
            <Modal.Title>Adiciona usuário</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Nome</Form.Label>
              <Form.Control type="text" onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, name: value}}))}}/>
              {
                  this.state.errors.name !== undefined && !this.state.errors.name.isValid &&
                  <Form.Text className="text-muted">
                    {this.state.errors.name.message}
                  </Form.Text> 
              }
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>CPF</Form.Label>
              <MaskedFormControl type='text' mask='111.111.111-11' onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, cpf: value}}))}}/>
              {
                  this.state.errors.cpf !== undefined && !this.state.errors.cpf.isValid &&
                  <Form.Text className="text-muted">
                    {this.state.errors.cpf.message}
                  </Form.Text> 
              }
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Data de Nascimento</Form.Label>
              <MaskedFormControl type='text' mask='11/11/1111' onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, dateOfBirth: value}}))}}/>
              {
                  this.state.errors.dateOfBirth !== undefined && !this.state.errors.dateOfBirth.isValid &&
                  <Form.Text className="text-muted">
                    {this.state.errors.dateOfBirth.message}
                  </Form.Text> 
              }
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, email: value}}))}}/>
              {
                  this.state.errors.email !== undefined && !this.state.errors.email.isValid &&
                  <Form.Text className="text-muted">
                    {this.state.errors.email.message}
                  </Form.Text> 
              }
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Naturalidade</Form.Label>
              <Form.Control type="text" onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, naturalness: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Nacionalidade</Form.Label>
              <Form.Control type="text" onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, nationality: value}}))}}/>
            </Form.Group>

            <Form.Group controlId="formGridState">
              <Form.Label>Sexo</Form.Label>
              <Form.Control as="select" defaultValue="F" onChange={(e) => {const value = e.target.value; this.setState(prevState => ({user: {...prevState.user, sex: value}}))}}>
                <option>F</option>
                <option>M</option>
              </Form.Control>
            </Form.Group>
          </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary"onClick={() => this.toggle()}>Fechar</Button>
            <Button variant="primary" onClick={() => this.save()}>Salvar</Button>
          </Modal.Footer>
        </Modal>
          </Row>  
      </Container>
    );
  }
}

export default AddUser;