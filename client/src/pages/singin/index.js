import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import api from "../../services/api";
import { login } from "../../services/auth";

import {Container, Row, Col, Form, Button} from "react-bootstrap";

class SignIn extends Component {
  state = {
    username: "",
    password: "",
    error: ""
  };

  handleSignIn = async e => {
    e.preventDefault();
    const { username, password } = this.state;
    var bodyFormData = new FormData();
    bodyFormData.set('username', username);
    bodyFormData.set('password', password);
    var config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    };

    if (!username || !password) {
      this.setState({ error: "Preencha o nome de usuário e senha para continuar!" });
    } else {
      await api.post(
        "/login", 
        bodyFormData, 
        config
      )
      .then(() => {
        login({credentials: username+':'+password});
        this.props.history.push("/app");
      })
      .catch((err) => {
        this.setState({
          error: "Houve um problema com o login, verifique suas credenciais."
        });
      });
    }
  };

  render() {
    return (
      <Container>
        <Row>
          <Col sm="12">
            <Form onSubmit={this.handleSignIn}>
            {this.state.error && <p>{this.state.error}</p>}
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Usuário</Form.Label>
                <Form.Control type="text" onChange={e => this.setState({ username: e.target.value })}/>
              </Form.Group>
              <Form.Group>
                <Form.Label>Senha</Form.Label>
                <Form.Control type="password" onChange={e => this.setState({ password: e.target.value })}/>
              </Form.Group>
              <Button sm="12" variant="primary" type="submit">Login</Button>
            </Form>
          </Col> 
        </Row>
      </Container>
    );
  }
}

export default withRouter(SignIn);