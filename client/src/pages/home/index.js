import React  from "react";
import { withRouter } from "react-router-dom";

import {Container} from "react-bootstrap";

import UserTable from "../../components/users";
import AddUser from "../../components/add-user";

class Home extends React.Component {

  render() {
    return (
      <Container>
        <br />
        <AddUser></AddUser>
        <br />
        <UserTable></UserTable>
      </Container>
    );
  }
}

export default withRouter(Home);