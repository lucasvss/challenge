package com.meta.application.controller;

import com.meta.application.domain.User;
import com.meta.application.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/users")
@RequiredArgsConstructor
public class UserController {
    
    private final UserService userService;
    
    @PostMapping
    @ApiOperation(value = "Retorna usuário criado")
    public User create(@RequestBody User user) {
        return userService.create(user);
    }
    
    @PutMapping()
    @ApiOperation(value = "Retorna usuário atualizado")
    public User update(@RequestBody User user) {
        return userService.update(user);
    }
    
    @GetMapping("/{id}/id")
    @ApiOperation(value = "Retorna usuário com o mesmo id informado na request")
    public User findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/{cpf}/cpf")
    @ApiOperation(value = "Retorna usuário com o mesmo cpf informado na request")
    public User findByCpf(@PathVariable String cpf) {
        return userService.findByCpf(cpf);
    }
    
    @GetMapping
    @ApiOperation(value = "Retorna todos usuários que não são admin")
    public List<User> find() {
        return userService.find();
    }
    
    @DeleteMapping("/{id}")
    @ApiOperation(value = "remove usuário da base de dados com o mesmo id informado na request")
    public void deleteById(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @GetMapping("/{name}/name")
    @ApiOperation(value = "Retorna usuário com o mesmo nome informado na request")
    public List<User> findByName(@PathVariable String name) {
        return userService.findByName(name);
    }
    
    @GetMapping("dateOfBirth/{dateOfBirth}/after")
    @ApiOperation(value = "Retorna todos os usuários que nasceram depois da data informada na request")
    public List<User> findByDateOfBirthAfter(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date dateOfBirth) { 
        return userService.findByDateOfBirthAfter(dateOfBirth); 
    }

    @GetMapping("dateOfBirth/{dateOfBirth}/before")
    @ApiOperation(value = "Retorna todos os usuários que nasceram antes da data informada na request")
    public List<User> findByDateOfBirthBefore(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date dateOfBirth) { 
        return userService.findByDateOfBirthBefore(dateOfBirth); 
    }

    @GetMapping("dateOfBirth/{beforeDate}/between/{afterDate}")
    @ApiOperation(value = "Retorna todos os usuários que nasceram entre as datas informadas na request")
    public List<User> findByDateOfBirthBetween(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date beforeDate, 
                                               @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date afterDate) { 
        return userService.findByDateOfBirthBetween(beforeDate, afterDate); 
    }
}
