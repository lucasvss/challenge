package com.meta.application.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/source")
public class SourceController {

    @GetMapping
    public String getLink() {
        return "<a href=\"https://github.com/lucasvss/challenge\">Link GitHub</a> <br /> " +
                "<a href=\"https://bitbucket.org/lucasvss/challenge/src/master/\">Link BitBucket</a>";
    }
}
