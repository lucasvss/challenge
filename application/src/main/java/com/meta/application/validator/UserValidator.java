package com.meta.application.validator;

import com.meta.application.domain.User;
import com.meta.application.domain.exception.EntityNotFoundException;
import com.meta.application.domain.exception.InvalidAttributeException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
public class UserValidator {

    public void validateCreate(User user) {
        validateNull(new EntityNotFoundException(), user);
        validateNotNull(new InvalidAttributeException(), user.getId());
        validateAttributes(user);
    }

    public void validateUpdate(User user) {
        validateNull(new EntityNotFoundException(), user);
        validateNull(new InvalidAttributeException(), user.getId());
        validateAttributes(user);
    }

    @SneakyThrows
    private void validateAttributes(User user) {
        if(isBlank(user.getName())) {
            throw new InvalidAttributeException("Name");
        }

        if(user.getDateOfBirth() == null) {
            throw new InvalidAttributeException("DateOfBirth");
        }

        if(isBlank(user.getCpf())) {
            throw new InvalidAttributeException("CPF");
        }
    }

    @SneakyThrows
    private void validateNull(Exception exception, Object object) {
        if (object == null) {
            throw exception;
        }
    }

    @SneakyThrows
    public void validateNotNull(Exception exception, Object object) {
        if (object != null) {
            throw exception;
        }
    }
    
}
