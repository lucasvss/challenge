package com.meta.application.config;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.meta.application.controller"))
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, responses())
                .globalResponseMessage(RequestMethod.POST, responses())
                .globalResponseMessage(RequestMethod.DELETE, responses())
                .globalResponseMessage(RequestMethod.PUT, responses());
    }
    
    private List<ResponseMessage> responses() {
        return Lists.newArrayList(
                new ResponseMessage(200, "Operação realizada com sucesso!", null,
                        Collections.emptyMap(),
                        Collections.emptyList()
                ),
                new ResponseMessage(500, "500 message", null,
                        Collections.emptyMap(), 
                        Collections.emptyList()
                ),
                new ResponseMessage(403, "Forbidden", null,
                        Collections.emptyMap(),
                        Collections.emptyList())
        );
    }
}
