package com.meta.application.domain.exception;

public class NullOrEmptyDataException extends ApplicationException {

    public NullOrEmptyDataException() { super("null.or.empty"); }

    public NullOrEmptyDataException(String message, Throwable cause) { super(message, cause); }

    public NullOrEmptyDataException(String message) { super(message); }
}
