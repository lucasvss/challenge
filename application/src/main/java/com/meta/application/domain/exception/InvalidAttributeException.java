package com.meta.application.domain.exception;

public class InvalidAttributeException extends ApplicationException {

    public InvalidAttributeException() { super("invalid.attribute");}

    public InvalidAttributeException(String resourceId, Throwable cause) {
        super(resourceId, cause);
    }

    public InvalidAttributeException(String resourceId) {
        super(resourceId);
    }
}
