package com.meta.application.domain.exception;

public class EntityNotFoundException extends ApplicationException {

    public EntityNotFoundException() { super("entity.notFound");}

    public EntityNotFoundException(String resourceId, Throwable cause) {
        super(resourceId, cause);
    }

    public EntityNotFoundException(String resourceId) {
        super(resourceId);
    }
}
