package com.meta.application.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Date;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {

    @Id
    @ApiModelProperty(value = "Código do usuário")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @ApiModelProperty(value = "Nome do usuário", required = true)
    private String name;
    
    @Column
    @ApiModelProperty(value = "Sexo do usuário")
    private String sex;
    
    @Column
    @ApiModelProperty(value = "Date de nascimento do usuário", required = true)
    private Date dateOfBirth;
    
    @Column
    @ApiModelProperty(value = "Naturalidade do usuário")
    private String naturalness;
    
    @Column
    @ApiModelProperty(value = "Nacionalidade do usuário")
    private String nationality;
    
    @CPF
    @Column
    @ApiModelProperty(value = "CPF do usuário", required = true)
    private String cpf;
    
    @Email
    @Column
    @ApiModelProperty(value = "Email do usuário")
    private String email;
    
    @Column
    @ApiModelProperty(value = "Senha do usuário")
    private String password;

    @Column
    @ApiModelProperty(value = "Data do cadastro do usuário")
    private Date creationDate;

    @Column
    @ApiModelProperty(value = "Data da atualização do cadastro do usuário")
    private Date lastModification;
    
    @Column
    @JsonIgnore
    private Boolean isAdmin;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) { return true; }
        if (id == null || obj == null || !getClass().equals(obj.getClass())) { return false; }
        return id.equals(((User) obj).id);
    }

    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

}
