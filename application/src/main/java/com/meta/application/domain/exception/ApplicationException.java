package com.meta.application.domain.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ApplicationException extends Exception {

    private static final long serialVersionUID = 7761714536944280396L;

    public ApplicationException() {
        super();
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(String message) {
        super(message);
    }

    @JsonIgnore
    @Override
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }
}
