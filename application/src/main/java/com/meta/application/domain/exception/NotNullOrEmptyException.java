package com.meta.application.domain.exception;

public class NotNullOrEmptyException extends ApplicationException {

    public NotNullOrEmptyException() { super("notNull.or.empty");}

    public NotNullOrEmptyException(String resourceId, Throwable cause) {
        super(resourceId, cause);
    }

    public NotNullOrEmptyException(String resourceId) {
        super(resourceId);
    }
}
