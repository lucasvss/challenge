package com.meta.application.service;

import com.meta.application.domain.User;
import com.meta.application.repository.UserRepository;
import com.meta.application.validator.UserValidator;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserValidator validator;
    private final UserRepository userRepository;

    public User create(User user) {
        preCreate(user);
        return userRepository.save(user);
    }

    public User update(User user) {
        preUpdate(user);
        return userRepository.save(user);
    }
    
    public void deleteById(Integer id) {
       userRepository.deleteById(id);
    }
    
    public List<User> find() {
        return userRepository.findByIsAdminNull();
    }
    
    public User findById(Integer id) {
        return userRepository.findById(id).orElse(null);
    }
    
    public User findByCpf(String cpf) {
        return userRepository.findByCpf(cpf);
    }
    
    public List<User> findByName(String name) {
        return userRepository.findByName(name);
    }
    
    public List<User> findByDateOfBirthAfter(Date date) {
        return userRepository.findByDateOfBirthAfter(date);
    }
    
    public List<User> findByDateOfBirthBefore(Date date) {
        return userRepository.findByDateOfBirthBefore(date);
    }
    
    public List<User> findByDateOfBirthBetween(Date beforeDate, Date afterDate) {
        return userRepository.findByDateOfBirthBetween(beforeDate, afterDate);
    }

    @SneakyThrows
    private void preCreate(User user) {
        validator.validateCreate(user);
        setCreationDate(user);
    }

    @SneakyThrows
    private void preUpdate(User user) {
        validator.validateUpdate(user);
        setLastModificationDate(user);
    }

    private void setCreationDate(User user) {
        user.setCreationDate(new Date());
    }

    private void setLastModificationDate(User user) {
        user.setLastModification(new Date());
    }
}
