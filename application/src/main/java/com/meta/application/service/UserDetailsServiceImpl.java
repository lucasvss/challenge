package com.meta.application.service;

import com.meta.application.domain.User;
import com.meta.application.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    
    private final UserRepository userRepository;
    
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String cpf) {
        User user = userRepository.findByCpf(cpf);
        
        if(user == null) {
            throw new UsernameNotFoundException(cpf);
        }
        return new org.springframework.security.core.userdetails.User(user.getCpf(), user.getPassword(), emptyList());
    }
}
