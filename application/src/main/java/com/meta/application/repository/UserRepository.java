package com.meta.application.repository;

import com.meta.application.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    
    User findByCpf(String cpf);
    List<User> findByIsAdminNull();
    List<User> findByName(String name);
    List<User> findByDateOfBirthAfter(Date dateOfBirth);
    List<User> findByDateOfBirthBefore(Date dateOfBirth);
    List<User> findByDateOfBirthBetween(Date beforeDate, Date afterDate);
}
