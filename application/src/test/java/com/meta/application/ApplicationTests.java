package com.meta.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.google.gson.Gson;
import com.meta.application.domain.User;
import com.meta.application.domain.exception.EntityNotFoundException;
import com.meta.application.domain.exception.InvalidAttributeException;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {
		"spring.liquibase.change-log=classpath:changelog/db/changelog-master.yml",
		"spring.liquibase.drop-first=true"
})
class ApplicationTests {

	private static final String NAME = UUID.randomUUID().toString();
	
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithIdNotNull() {
		User user = createUser(10000,null, "00792071468", new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}
	
	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithNameNull() {
		User user = createUser(null,null, "00792071468", new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithNameEmpty() {
		User user = createUser(null,"", "00792071468", new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithCpfNull() {
		User user = createUser(null, NAME, null, new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithCpfEmpty() {
		User user = createUser(null, NAME, "", new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithCpfInvalid() {
		User user = createUser(null, NAME, "11111111111", new Date());
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof ConstraintViolationException));
	}

	@Test
	@SneakyThrows
	void itNotShouldCreateUserWithDateOfBirthNull() {
		User user = createUser(null, NAME, "00792071468", null);
		mockMvc.perform(post("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}
	
	@Test
	@SneakyThrows
	void itShouldCreateUser() {
		User user = createUser(null, NAME, "39557929600", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithIdNull() {
		User user = createUser(null,null, "00792071468", new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithNameNull() {
		User user = createUser(10000,null, "00792071468", new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithNameEmpty() {
		User user = createUser(10000,"", "00792071468", new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithCpfNull() {
		User user = createUser(10000, NAME, null, new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithCpfEmpty() {
		User user = createUser(10000, NAME, "", new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithCpfInvalid() {
		User user = createUser(10000, NAME, "11111111111", new Date());
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof ConstraintViolationException));
	}

	@Test
	@SneakyThrows
	void itNotShouldUpdateUserWithDateOfBirthNull() {
		User user = createUser(10000, NAME, "00792071468", null);
		mockMvc.perform(put("/api/users").header(HttpHeaders.AUTHORIZATION,
				"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isInternalServerError())
				.andExpect(result -> Assert.assertTrue(result.getResolvedException() instanceof InvalidAttributeException));
	}
	
	@Test
	@SneakyThrows
	void itShouldUpdateUser() {
		User user = createUser(null, NAME, "75645357418", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User updatedUser = gson.fromJson(mockMvc.perform(put("/api/users")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(createdUser)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(updatedUser);
		Assert.assertNotNull(updatedUser.getLastModification());
	}

	@Test
	@SneakyThrows
	void itShouldFindById() {
		User user = createUser(null, NAME, "84575135836", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User findUser = gson.fromJson(mockMvc.perform(get("/api/users/" + createdUser.getId() + "/id")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(user);
		Assert.assertEquals(createdUser.getId(), findUser.getId());
	}

	@Test
	@SneakyThrows
	void itShouldFindByName() {
		User user = createUser(null, NAME, "24218206007", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users/")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getName());
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User[] findUsers = gson.fromJson(mockMvc.perform(get("/api/users/" + createdUser.getName() + "/name")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User[].class);

		Assert.assertNotNull(user);
		Assert.assertNotEquals(0, findUsers.length);
	}

	@Test
	@SneakyThrows
	void itShouldFindByCpf() {
		User user = createUser(null, NAME, "61663455856", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getCpf());
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User findUser = gson.fromJson(mockMvc.perform(get("/api/users/" + createdUser.getCpf() + "/cpf")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(user);
		Assert.assertEquals(createdUser.getCpf(), findUser.getCpf());
	}

	@Test
	@SneakyThrows
	void itShouldFindByDateOfBirthAfter() {
		User user = createUser(null, NAME, "54885757231", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users/")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getName());
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User[] findUsers = gson.fromJson(mockMvc.perform(get("/api/users/dateOfBirth/1996-03-11 00:00:00/after")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User[].class);

		Assert.assertNotNull(user);
		Assert.assertNotEquals(0, findUsers.length);
	}

	@Test
	@SneakyThrows
	void itShouldFindByDateOfBirthBefore() {
		User user = createUser(null, NAME, "62271255120", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users/")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getName());
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User[] findUsers = gson.fromJson(mockMvc.perform(get("/api/users/dateOfBirth/1996-03-11 00:00:00/before")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User[].class);

		Assert.assertNotNull(user);
		Assert.assertEquals(0, findUsers.length);
	}

	@Test
	@SneakyThrows
	void itShouldFindByDateOfBirthBetween() {
		User user = createUser(null, NAME, "98735950820", new Date());
		Gson gson = new Gson();
		User createdUser = gson.fromJson(mockMvc.perform(post("/api/users/")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User.class);

		Assert.assertNotNull(createdUser);
		Assert.assertNotNull(createdUser.getName());
		Assert.assertNotNull(createdUser.getCreationDate());
		Assert.assertNull(createdUser.getLastModification());

		User[] findUsers = gson.fromJson(mockMvc.perform(get("/api/users/dateOfBirth/1996-03-11 00:00:00/between/2020-01-01 00:00:00")
				.header(HttpHeaders.AUTHORIZATION,
						"Basic " + Base64Utils.encodeToString("admin:admin".getBytes()))
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString(), User[].class);

		Assert.assertNotNull(user);
		Assert.assertEquals(0, findUsers.length);
	}
	
	private User createUser(Integer id, String name, String cpf, Date dateOfBirth) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		user.setCpf(cpf);
		user.setDateOfBirth(dateOfBirth);
		user.setSex("M");
		user.setNationality("Brazilian");
		user.setNaturalness("Fortaleza-CE");
		return user;
	}

}
